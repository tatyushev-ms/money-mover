package com.revolut.servlet;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.revolut.DbDataInitializer;
import com.revolut.JdbcConnectionPoolHolder;
import com.revolut.Main;
import com.revolut.dto.BadRequestResult;
import org.apache.catalina.startup.Tomcat;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

import static com.revolut.servlet.TestUtils.assertCollectionsAlike;
import static org.junit.jupiter.api.Assertions.*;

class ApiIT {

    private final String baseUrl = "http://localhost:8080/money-mover";
    private final Gson gson = new Gson();

    private static Tomcat tomcat;

    @BeforeAll
    static void setUp() throws Exception {
        System.out.println("############################## AccountServletIT.setUp");
        tomcat = Main.configureApp(null);
        tomcat.start();
    }

    @AfterAll
    static void tearDown() throws Exception {
        System.out.println("############################## AccountServletIT.tearDown");
        Main.dispose();
        tomcat.stop();
    }

    @Nested
    class AccountServletIT {

        @Test
        void getCollectionResource() throws UnirestException {
            System.out.println("############################## AccountServletIT.getCollectionResource");
            resetDbData();

            final HttpResponse<String> response = Unirest.get(baseUrl + "/account" + "/")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .asString();

            assertEquals(200, response.getStatus());
            assertEquals("[{\"id\":1,\"balance\":10},{\"id\":2,\"balance\":20}]", response.getBody());
        }

        @Test
        void getMemberResource() throws UnirestException {
            System.out.println("############################## AccountServletIT.getMemberResource");
            resetDbData();

            final String accountId = "1";
            final HttpResponse<String> response = getAccount(accountId);

            assertEquals(200, response.getStatus());
            assertEquals("{\"id\":" + accountId + ",\"balance\":10}", response.getBody());
        }

        @Test
        void accountNotFound() throws UnirestException {
            System.out.println("############################## AccountServletIT.accountNotFound");
            resetDbData();

            final String accountId = "3";
            final HttpResponse<String> response = getAccount(accountId);

            assertEquals(404, response.getStatus());
            assertTrue(response.getBody().isEmpty());
        }

        @Test
        void notNumericId() throws UnirestException {
            System.out.println("############################## AccountServletIT.notNumericId");
            resetDbData();

            final String accountId = "374a";
            final HttpResponse<String> response = getAccount(accountId);

            assertEquals(400, response.getStatus());
            assertTrue(response.getBody().isEmpty());
        }

    }

    @Nested
    class TransferServletIT {

        @Test
        void badRequest() throws UnirestException {
            System.out.println("############################## TransferServletIT.badRequest");
            resetDbData();

            badTransferRequest("{\"sum\":123.56, \"romAccountId\":1, \"toAccountId\":2}", "\"fromAccountId is a mandatory attribute\"");
            badTransferRequest("{\"bum\":123.56, \"fromAccountId\":1, \"toAccountId\":2}", "\"sum is a mandatory attribute\"");
            badTransferRequest("{\"sum\":123.56, \"fromAccountId\":1, \"oAccountId\":2}", "\"toAccountId is a mandatory attribute\"");
        }

        @Test
        void badRequestMultipleErrors() throws UnirestException {
            System.out.println("############################## TransferServletIT.badRequestMultipleErrors");
            resetDbData();

            final HttpResponse<String> response = Unirest.post(baseUrl + "/transfer")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .body("{\"rum\":123.56, \"promAccountId\":1, \"goAccountId\":2}")
                    .asString();

            assertEquals(400, response.getStatus());
            final String body = response.getBody();
            assertFalse(body.isEmpty());
            final BadRequestResult result = gson.fromJson(body, BadRequestResult.class);
            assertCollectionsAlike(
                    Arrays.asList("fromAccountId is a mandatory attribute", "sum is a mandatory attribute", "toAccountId is a mandatory attribute"),
                    new ArrayList<>(result.getMessages())
            );
        }

        @Test
        void notEnoughMoney() throws UnirestException {
            System.out.println("############################## TransferServletIT.notEnoughMoney");
            resetDbData();

            final HttpResponse<String> response = transfer("11", "1", "2");

            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Not enough money\"}", response.getBody());
        }

        @Test
        void incorrectAmount() throws UnirestException {
            System.out.println("############################## TransferServletIT.incorrectAmount");
            resetDbData();

            HttpResponse<String> response;

            response = transfer("0", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer("0.0", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer("+0.0", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer("-0.0", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer("-0.1", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer(".0", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());

            response = transfer("-1", "1", "2");
            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Incorrect amount\"}", response.getBody());
        }

        @Test
        void fromAndToAccountsTheSame() throws UnirestException {
            System.out.println("############################## TransferServletIT.fromAndToAccountsTheSame");
            resetDbData();

            final HttpResponse<String> response = transfer("11", "1", "1");

            assertEquals(200, response.getStatus());
            assertEquals("{\"success\":false,\"message\":\"From and to accounts are the same\"}", response.getBody());
        }

        @Test
        void accountNotFound() throws UnirestException {
            System.out.println("############################## TransferServletIT.accountNotFound");
            resetDbData();

            String unknownAccount1 = "3";
            final HttpResponse<String> response1 = transfer("5", "1", unknownAccount1);
            assertEquals(200, response1.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Account not found: " + unknownAccount1 + "\"}", response1.getBody());

            String unknownAccount2 = "4";
            final HttpResponse<String> response2 = transfer("5", unknownAccount2, "1");
            assertEquals(200, response2.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Account not found: " + unknownAccount2 + "\"}", response2.getBody());

            unknownAccount1 = "6";
            unknownAccount2 = "7";
            final HttpResponse<String> response3 = transfer("5", unknownAccount1, unknownAccount2);
            assertEquals(200, response3.getStatus());
            assertEquals("{\"success\":false,\"message\":\"Account not found: " + unknownAccount1 + "\"}", response3.getBody());
        }

        @Test
        void multipleTransfersInRow() throws Exception {
            System.out.println("############################## TransferServletIT.multipleTransfersInRow");
            resetDbData();

            final CompletableFuture<HttpResponse<String>> request0 = transferRequest("1", "2", "1");
            request0.join();
            checkAccountBalance("1", "11");
            checkAccountBalance("2", "19");

            final CompletableFuture<HttpResponse<String>> request1 = transferRequest("5", "1", "2");
            request1.join();
            checkAccountBalance("1", "6");
            checkAccountBalance("2", "24");

            final CompletableFuture<HttpResponse<String>> request2 = transferRequest("3", "2", "1");
            request2.join();
            checkAccountBalance("1", "9");
            checkAccountBalance("2", "21");

            final CompletableFuture<HttpResponse<String>> request3 = transferRequest("6", "2", "1");
            request3.join();
            checkAccountBalance("1", "15");
            checkAccountBalance("2", "15");

            final CompletableFuture<HttpResponse<String>> request4 = transferRequest("2", "1", "2");
            request4.join();
            checkAccountBalance("1", "13");
            checkAccountBalance("2", "17");
        }

        @Test
        void multipleTransfersSimultaneously() throws Exception {
            System.out.println("############################## TransferServletIT.multipleTransfersSimultaneously");
            resetDbData();

            final CompletableFuture<HttpResponse<String>> request0 = transferRequest("1", "2", "1");
            final CompletableFuture<HttpResponse<String>> request1 = transferRequest("5", "1", "2");
            final CompletableFuture<HttpResponse<String>> request2 = transferRequest("3", "2", "1");
            final CompletableFuture<HttpResponse<String>> request3 = transferRequest("6", "2", "1");
            final CompletableFuture<HttpResponse<String>> request4 = transferRequest("2", "1", "2");

            CompletableFuture.allOf(request0, request1, request2, request3, request4).join();

            checkAccountBalance("1", "13");
            checkAccountBalance("2", "17");
        }

        private void checkAccountBalance(String id, String requiredBalance) throws UnirestException {
            final HttpResponse<String> response = getAccount(id);
            assertEquals(200, response.getStatus());
            assertEquals("{\"id\":" + id + ",\"balance\":" + requiredBalance + "}", response.getBody());
        }

        private HttpResponse<String> transfer(String sum, String from, String to) throws UnirestException {
            return Unirest.post(baseUrl + "/transfer")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .body("{\"sum\":" + sum + ", \"fromAccountId\":" + from + ", \"toAccountId\":" + to + "}")
                    .asString();
        }

        private CompletableFuture<HttpResponse<String>> transferRequest(String sum, String from, String to) {
            return CompletableFuture.supplyAsync(() -> {
                try {
                    return transfer(sum, from, to);
                } catch (UnirestException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        private void badTransferRequest(String body, String expectedMessage) throws UnirestException {
            final HttpResponse<String> response = Unirest.post(baseUrl + "/transfer")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .body(body)
                    .asString();

            assertEquals(400, response.getStatus());
            assertEquals("{\"messages\":[" + expectedMessage + "]}", response.getBody());
        }

    }

    private HttpResponse<String> getAccount(String id) throws UnirestException {
        return Unirest.get(baseUrl + "/account" + "/" + id)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .asString();
    }

    private void resetDbData() {
        final JdbcConnectionPool cp = JdbcConnectionPoolHolder.getJdbcConnectionPoolInstance();
        final DbDataInitializer dbDataInitializer = new DbDataInitializer(cp);
        dbDataInitializer.initialize();
    }

}
