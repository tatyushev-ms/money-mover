package com.revolut.servlet;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValidationResultTest {

    private final ValidationResult validationResult = new ValidationResult();

    @Test
    void valid() {
        assertThatValid();
        assertWithoutMessages();
    }

    @Test
    void withError() {
        addError("Text");

        assertThatInvalid();
        assertThatContainsMessages("Text");
    }

    @Test
    void withMultipleErrors() {
        addError("Text");
        addError("Line");

        assertThatInvalid();
        assertThatContainsMessages("Text", "Line");
    }

    private void assertThatValid() {
        assertTrue(validationResult.isValid());
    }

    private void assertWithoutMessages() {
        assertTrue(validationResult.getMessages().isEmpty());
    }

    private void assertThatInvalid() {
        assertFalse(validationResult.isValid());
    }

    private void addError(String message) {
        validationResult.addError(message);
    }

    private void assertThatContainsMessages(String... messages) {
        assertThatContainsMessages(Arrays.asList(messages));
    }

    private void assertThatContainsMessages(List<String> messages) {
        TestUtils.assertCollectionsAlike(messages, validationResult.getMessages());
    }

}