package com.revolut.servlet;

import com.revolut.ResponseShaper;
import com.revolut.domain.Account;
import com.revolut.dao.AccountDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Arrays;
import java.util.Collection;

import static com.revolut.servlet.DomainUtils.account;
import static com.revolut.servlet.DomainUtils.bg;
import static org.mockito.Mockito.*;

class AccountServletTest {

    private HttpServletRequest req;
    private HttpServletResponse resp;

    private ResponseShaper responseShaper;
    private AccountDao accountDao;

    private AccountServlet accountServlet;

    @BeforeEach
    void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
        responseShaper = mock(ResponseShaper.class);
        accountDao = mock(AccountDao.class);

        accountServlet = new AccountServlet(responseShaper, accountDao);
    }

    @Test
    void getCollectionResourceWithError() throws Exception {
        when(accountDao.getAll()).thenThrow(new RuntimeException());
        when(req.getPathInfo()).thenReturn("/");
        accountServlet.getCollectionResource(req, resp);

        verify(responseShaper).respondWithStatus(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    void getCollectionResource() throws Exception {
        final Collection<Account> expectedResult = Arrays.asList(account(123, bg("234234.2352")), account(456, bg("1.78")));

        when(accountDao.getAll()).thenReturn(expectedResult);
        when(req.getPathInfo()).thenReturn("/");
        accountServlet.getCollectionResource(req, resp);

        verify(responseShaper).sendAsJson(resp, expectedResult);
    }

}