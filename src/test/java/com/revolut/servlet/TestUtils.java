package com.revolut.servlet;

import org.opentest4j.AssertionFailedError;

import java.util.Collection;

import static org.junit.platform.commons.util.Preconditions.notNull;

public final class TestUtils {

    private TestUtils() {
        throw new IllegalStateException();
    }

    public static void assertCollectionsAlike(Collection<String> expectedLines, Collection<String> actualLines) {
        notNull(expectedLines, "expectedLines must not be null");
        notNull(actualLines, "actualLines must not be null");

        // trivial case: same list instance
        if (expectedLines == actualLines) {
            return;
        }

        int expectedSize = expectedLines.size();
        int actualSize = actualLines.size();

        if (expectedSize != actualSize) {
            throw new AssertionFailedError("Collections are not alike. Sizes are different", expectedLines.toString(), actualLines.toString());
        }
        for (String i : expectedLines) {
            if (!actualLines.contains(i)) {
                throw new AssertionFailedError("Collections are ont alike", expectedLines.toString(), actualLines.toString());
            }
        }
    }

}
