package com.revolut.servlet;

import com.google.gson.Gson;
import com.revolut.ResponseShaper;
import com.revolut.dao.AccountDao;
import com.revolut.dto.Result;
import com.revolut.dto.TransferRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import static com.revolut.servlet.DomainUtils.bg;
import static org.mockito.Mockito.*;

class TransferServletTest {

    private HttpServletRequest req;
    private HttpServletResponse resp;

    private Gson gson = new Gson();
    private Validator validator;
    private ResponseShaper responseShaper;
    private AccountDao accountDao;

    private TransferServlet transferServlet;

    @BeforeEach
    void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
        validator = mock(Validator.class);
        responseShaper = mock(ResponseShaper.class);
        accountDao = mock(AccountDao.class);

        transferServlet = new TransferServlet(gson, validator, responseShaper, accountDao);
    }

    @Test
    void transfer() throws ServletException, IOException {
        final TransferRequest tr = new TransferRequest(bg("9.7"), 1L, 2L);

        when(req.getReader()).thenReturn(new BufferedReader(new StringReader("{\"sum\":9.7, \"fromAccountId\":1, \"toAccountId\":2}")));
        when(validator.validate(any())).thenReturn(new ValidationResult());

        transferServlet.doPost(req, resp);

        verify(responseShaper).sendAsJson(resp, Result.success());
    }

}