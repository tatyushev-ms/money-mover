package com.revolut.servlet;

import com.revolut.dto.TransferRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static com.revolut.servlet.DomainUtils.bg;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidatorTest {

    private final Validator validator = new Validator();

    private ValidationResult validationResult;

    @Test
    void nothing() {
        buildRequest(null, null, null);

        assertThatInvalid();
        assertThatContainsMessages("toAccountId is a mandatory attribute", "sum is a mandatory attribute", "fromAccountId is a mandatory attribute");
    }

    @Test
    void withoutSomething() {
        buildRequest(null, 8L, 3L);
        assertThatInvalid();
        assertThatContainsMessages("sum is a mandatory attribute");

        buildRequest(bg("3.5"), null, 3L);
        assertThatInvalid();
        assertThatContainsMessages("fromAccountId is a mandatory attribute");

        buildRequest(bg("3.5"), 8L, null);
        assertThatInvalid();
        assertThatContainsMessages("toAccountId is a mandatory attribute");

        buildRequest(null, null, 3L);
        assertThatInvalid();
        assertThatContainsMessages("sum is a mandatory attribute", "fromAccountId is a mandatory attribute");

        buildRequest(null, 8L, null);
        assertThatInvalid();
        assertThatContainsMessages("toAccountId is a mandatory attribute", "sum is a mandatory attribute");

        buildRequest(bg("3.5"), null, null);
        assertThatInvalid();
        assertThatContainsMessages("toAccountId is a mandatory attribute", "fromAccountId is a mandatory attribute");
    }

    @Test
    void valid() {
        buildRequest(bg("3.5"), 8L, 3L);

        assertThatValid();
    }

    private void assertThatValid() {
        assertTrue(validationResult.isValid());
    }

    private void assertThatContainsMessages(String... messages) {
        assertThatContainsMessages(Arrays.asList(messages));
    }

    private void assertThatContainsMessages(List<String> messages) {
        TestUtils.assertCollectionsAlike(messages, validationResult.getMessages());
    }

    private void assertThatInvalid() {
        assertFalse(validationResult.isValid());
    }

    private void buildRequest(BigDecimal sum, Long fromAccountId, Long toAccountId) {
        validationResult = validator.validate(tr(sum, fromAccountId, toAccountId));
    }

    private TransferRequest tr(BigDecimal sum, Long fromAccountId, Long toAccountId) {
        return new TransferRequest(sum, fromAccountId, toAccountId);
    }

}