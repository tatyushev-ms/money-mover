package com.revolut.servlet;

import com.revolut.domain.Account;

import java.math.BigDecimal;

public final class DomainUtils {

    private DomainUtils() {
        throw new IllegalStateException();
    }

    public static Account account(long id, BigDecimal balance) {
        return new Account(id, balance);
    }

    public static BigDecimal bg(String value) {
        return new BigDecimal(value);
    }

}
