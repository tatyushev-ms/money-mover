package com.revolut;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.h2.jdbcx.JdbcConnectionPool;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DbDataInitializer {

    private static final String SQL_TABLE_CREATE = "CREATE TABLE ACCOUNT(ID BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY, BALANCE DECIMAL NOT NULL)";//CREATED_DATE timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
    private static final String SQL_TABLE_DROP = "DROP TABLE IF EXISTS ACCOUNT";

    private static final String SQL_INSERT = "INSERT INTO ACCOUNT (BALANCE) VALUES (?)";

    private final Log log = LogFactory.getLog(DbDataInitializer.class);

    private final JdbcConnectionPool jdbcConnectionPool;

    public DbDataInitializer(JdbcConnectionPool jdbcConnectionPool) {
        this.jdbcConnectionPool = jdbcConnectionPool;
    }

    public void initialize() {
        Connection connection = null;
        Statement statement = null;
        PreparedStatement insertStatement = null;
        try {
            connection = jdbcConnectionPool.getConnection();
            statement = connection.createStatement();

            statement.execute(SQL_TABLE_DROP);
            statement.execute(SQL_TABLE_CREATE);

            connection.setAutoCommit(false);

            insertStatement = connection.prepareStatement(SQL_INSERT);
            insertStatement.setBigDecimal(1, new BigDecimal(10));
            insertStatement.execute();

            insertStatement.setBigDecimal(1, new BigDecimal(20));
            insertStatement.execute();

            connection.commit();

            connection.setAutoCommit(true);
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            closeStatement(statement);
            closeStatement(insertStatement);
            closeConnection(connection);
        }
    }

    private void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

}
