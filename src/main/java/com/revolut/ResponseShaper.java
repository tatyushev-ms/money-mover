package com.revolut;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface ResponseShaper {

    void sendAsJson(HttpServletResponse resp, Object obj) throws IOException;

    void respondWithStatus(HttpServletResponse resp, int status) throws IOException;

    void respondWithStatusAndJsonBody(HttpServletResponse resp, int status, Object obj) throws IOException;

}
