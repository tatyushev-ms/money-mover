package com.revolut;

import org.h2.jdbcx.JdbcConnectionPool;

public class JdbcConnectionPoolHolder {

    private static JdbcConnectionPool instance = null;

    private JdbcConnectionPoolHolder() {
    }

    public static JdbcConnectionPool getJdbcConnectionPoolInstance() {
        return getJdbcConnectionPoolInstance(false);
    }

    public static JdbcConnectionPool getJdbcConnectionPoolInstance(boolean forceNew) {
        if (instance == null) {
            synchronized (JdbcConnectionPoolHolder.class) {
                if (instance == null || forceNew) {
                    instance = JdbcConnectionPool.create("jdbc:h2:mem:test;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=TRUE", "sa", "sa");
                }
            }
        }
        return instance;
    }

}
