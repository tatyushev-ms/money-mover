package com.revolut.servlet;

import com.revolut.dto.TransferRequest;

import java.math.BigDecimal;

public class Validator {

    public ValidationResult validate(TransferRequest tr) {
        final ValidationResult result = new ValidationResult();

        final Long fromAccountId = tr.getFromAccountId();
        final Long toAccountId = tr.getToAccountId();
        final BigDecimal sum = tr.getSum();
        if (sum == null) {
            result.addError("sum is a mandatory attribute");
        }
        if (fromAccountId == null) {
            result.addError("fromAccountId is a mandatory attribute");
        }
        if (toAccountId == null) {
            result.addError("toAccountId is a mandatory attribute");
        }
        return result;
    }

}
