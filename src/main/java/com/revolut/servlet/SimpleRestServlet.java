package com.revolut.servlet;

import com.revolut.ResponseShaper;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SimpleRestServlet extends HttpServlet {

    private final Log log = LogFactory.getLog(getClass());

    protected final ResponseShaper responseShaper;

    public SimpleRestServlet(ResponseShaper responseShaper) {
        this.responseShaper = responseShaper;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String pathInfo = req.getPathInfo();
        if (pathInfo == null || pathInfo.equals("/")) {
            getCollectionResource(req, resp);
            return;
        }
        final String[] splits = pathInfo.split("/");
        if (splits.length != 2) {
            responseShaper.respondWithStatus(resp, HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        final String idAsString = splits[1];
        getMemberResource(req, idAsString, resp);
    }

    protected void getCollectionResource(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    protected void getMemberResource(HttpServletRequest req, String idAsString, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

}
