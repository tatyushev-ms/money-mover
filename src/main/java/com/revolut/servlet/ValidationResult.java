package com.revolut.servlet;

import java.util.HashSet;
import java.util.Set;

public class ValidationResult {

    private Set<String> messages;

    public boolean isValid() {
        return messages == null || messages.isEmpty();
    }

    public void addError(String message) {
        if (messages == null) {
            messages = new HashSet<>();
        }
        messages.add(message);
    }

    public Set<String> getMessages() {
        if (messages == null) {
            messages = new HashSet<>();
        }
        return messages;
    }

}
