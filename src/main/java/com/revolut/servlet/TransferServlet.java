package com.revolut.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.revolut.ResponseShaper;
import com.revolut.dao.*;
import com.revolut.domain.Transfer;
import com.revolut.dto.BadRequestResult;
import com.revolut.dto.Result;
import com.revolut.dto.TransferRequest;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class TransferServlet extends HttpServlet {

    private final Log log = LogFactory.getLog(getClass());

    private final Gson gson;
    private final Validator validator;
    private final ResponseShaper responseShaper;
    private final AccountDao accountDao;

    public TransferServlet(Gson gson, Validator validator, ResponseShaper responseShaper, AccountDao accountDao) {
        this.gson = gson;
        this.validator = validator;
        this.responseShaper = responseShaper;
        this.accountDao = accountDao;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        log.info("body = " + body);

        final TransferRequest tr;
        try {
            tr = gson.fromJson(body, TransferRequest.class);
        } catch (JsonSyntaxException e) {
            log.error(e.getMessage());
            responseShaper.respondWithStatus(resp, HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        final ValidationResult validationResult = validator.validate(tr);
        if (!validationResult.isValid()) {
            responseShaper.respondWithStatusAndJsonBody(resp, HttpServletResponse.SC_BAD_REQUEST, new BadRequestResult(validationResult.getMessages()));
        }
        try {
            accountDao.transferMoney(new Transfer(tr.getSum(), tr.getFromAccountId(), tr.getToAccountId()));
        } catch (RuntimeException e) {
            handleTransferException(req, resp, e);
            return;
        }
        responseShaper.sendAsJson(resp, Result.success());
    }

    private void handleTransferException(HttpServletRequest req, HttpServletResponse resp, RuntimeException e) throws IOException {
        if (e instanceof AccountNotFoundException) {
            responseShaper.sendAsJson(resp, Result.failure("Account not found: " + ((AccountNotFoundException) e).getAccountId()));
            return;
        }
        if (e instanceof FromAndToAccountsTheSameException) {
            responseShaper.sendAsJson(resp, Result.failure("From and to accounts are the same"));
            return;
        }
        if (e instanceof NotEnoughMoneyException) {
            responseShaper.sendAsJson(resp, Result.failure("Not enough money"));
            return;
        }
        if (e instanceof IncorrectAmountException) {
            responseShaper.sendAsJson(resp, Result.failure("Incorrect amount"));
            return;
        }
        log.error(e.getMessage());
    }

}
