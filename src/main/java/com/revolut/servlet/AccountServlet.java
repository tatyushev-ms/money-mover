package com.revolut.servlet;

import com.revolut.ResponseShaper;
import com.revolut.dao.AccountDao;
import com.revolut.dao.AccountNotFoundException;
import com.revolut.domain.Account;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class AccountServlet extends SimpleRestServlet {

    private final Log log = LogFactory.getLog(getClass());

    private final AccountDao accountDao;

    public AccountServlet(ResponseShaper responseShaper, AccountDao accountDao) {
        super(responseShaper);
        this.accountDao = accountDao;
    }

    @Override
    protected void getCollectionResource(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("collection");
        final Collection<Account> accounts;
        try {
            accounts = accountDao.getAll();
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            responseShaper.respondWithStatus(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        responseShaper.sendAsJson(resp, accounts);
    }

    @Override
    protected void getMemberResource(HttpServletRequest req, String idAsString, HttpServletResponse resp) throws ServletException, IOException {
        final long id;
        try {
            id = Long.parseLong(idAsString);
        } catch (NullPointerException | IllegalArgumentException e) {
            responseShaper.respondWithStatus(resp, HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        log.info("id = " + id);
        final Account account;
        try {
            account = accountDao.getById(id);
        } catch (RuntimeException e) {
            if (e instanceof AccountNotFoundException) {
                responseShaper.respondWithStatus(resp, HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            log.error(e.getMessage());
            responseShaper.respondWithStatus(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        responseShaper.sendAsJson(resp, account);
    }

}
