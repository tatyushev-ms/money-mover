package com.revolut.dto;

import java.util.Collection;

public class BadRequestResult {

    private final Collection<String> messages;

    public BadRequestResult(Collection<String> messages) {
        this.messages = messages;
    }

    public Collection<String> getMessages() {
        return messages;
    }

}