package com.revolut.dto;

import java.math.BigDecimal;

public class TransferRequest {

    private final BigDecimal sum;
    private final Long fromAccountId;
    private final Long toAccountId;

    public TransferRequest(BigDecimal sum, Long fromAccountId, Long toAccountId) {
        this.sum = sum;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

}
