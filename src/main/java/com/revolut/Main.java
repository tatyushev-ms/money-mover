package com.revolut;

import com.google.gson.Gson;
import com.revolut.dao.AccountDao;
import com.revolut.dao.SimpleAccountDao;
import com.revolut.servlet.AccountServlet;
import com.revolut.servlet.TransferServlet;
import com.revolut.servlet.Validator;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.h2.jdbcx.JdbcConnectionPool;

import javax.servlet.http.HttpServlet;

public class Main {

    public static void main(String[] args) throws Exception {
        final Tomcat tomcat = configureApp(args);

        tomcat.start();
        tomcat.getServer().await();
        dispose();
    }

    public static Tomcat configureApp(String[] args) throws Exception {
        final Tomcat tomcat = new Tomcat();

        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }

        tomcat.setPort(Integer.parseInt(webPort));

        final String contextPath = "/money-mover";
        final StandardContext context = (StandardContext) tomcat.addContext(contextPath, null);
        context.setResponseCharacterEncoding("UTF-8");

        final Gson gson = new Gson();

        final JdbcConnectionPool cp = JdbcConnectionPoolHolder.getJdbcConnectionPoolInstance();
        final DbDataInitializer dbDataInitializer = new DbDataInitializer(cp);
        dbDataInitializer.initialize();

        final AccountDao accountDao = new SimpleAccountDao(cp);

        final ResponseShaper responseShaper = new SimpleResponseShaper(gson);
        final Validator validator = new Validator();

        final TransferServlet transferServlet = new TransferServlet(gson, validator, responseShaper, accountDao);
        addTransferServlet(tomcat, context, contextPath, transferServlet);

        final AccountServlet accountServlet = new AccountServlet(responseShaper, accountDao);
        addAccountServlet(tomcat, context, contextPath, accountServlet);

        return tomcat;
    }

    public static void dispose() {
        JdbcConnectionPoolHolder.getJdbcConnectionPoolInstance().dispose();
    }

    private static void addTransferServlet(Tomcat tomcat, StandardContext context, String contextPath, TransferServlet transferServlet) {
        final String servletName = "TransferServlet";
        final String urlPattern = "/transfer";
        addServlet(tomcat, context, contextPath, servletName, urlPattern, transferServlet);
    }

    private static void addAccountServlet(Tomcat tomcat, StandardContext context, String contextPath, AccountServlet accountServlet) {
        final String servletName = "AccountServlet";
        final String urlPattern = "/account/*";
        addServlet(tomcat, context, contextPath, servletName, urlPattern, accountServlet);
    }

    private static void addServlet(Tomcat tomcat, StandardContext context, String contextPath, String servletName, String urlPattern, HttpServlet servlet) {
        tomcat.addServlet(contextPath, servletName, servlet);
        context.addServletMappingDecoded(urlPattern, servletName);
    }

}
