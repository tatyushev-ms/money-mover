package com.revolut.domain;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {

    private final long id;
    private final BigDecimal balance;

    public Account(long id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                balance.equals(account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
