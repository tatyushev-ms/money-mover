package com.revolut.domain;

import java.math.BigDecimal;

public class Transfer {

    private final BigDecimal sum;
    private final long fromAccountId;
    private final long toAccountId;

    public Transfer(BigDecimal sum, long fromAccountId, long toAccountId) {
        this.sum = sum;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public long getFromAccountId() {
        return fromAccountId;
    }

    public long getToAccountId() {
        return toAccountId;
    }

}
