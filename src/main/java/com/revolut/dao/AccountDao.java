package com.revolut.dao;

import com.revolut.domain.Account;
import com.revolut.domain.Transfer;

import java.util.Collection;

public interface AccountDao {

    Account getById(long id);

    void transferMoney(Transfer transfer);

    Collection<Account> getAll();

    //void deposit(Deposit deposit);//long id, BigDecimal sum

    //void withdraw(Withdrawal withdrawal);//long id, BigDecimal sum

}
