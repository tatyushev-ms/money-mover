package com.revolut.dao;

import java.math.BigDecimal;

public class NotEnoughMoneyException extends RuntimeException {

    private final long accountId;
    private final BigDecimal accountBalance;
    private final BigDecimal requiredBalance;

    public NotEnoughMoneyException(long accountId, BigDecimal accountBalance, BigDecimal requiredBalance) {
        this.accountId = accountId;
        this.accountBalance = accountBalance;
        this.requiredBalance = requiredBalance;
    }

    public long getAccountId() {
        return accountId;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public BigDecimal getRequiredBalance() {
        return requiredBalance;
    }

}
