package com.revolut.dao;

import com.revolut.domain.Account;
import com.revolut.domain.Transfer;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Collection;
import java.util.LinkedHashSet;

public class SimpleAccountDao implements AccountDao {

    private final Log log = LogFactory.getLog(getClass());

    private final DataSource dataSource;

    public SimpleAccountDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Account getById(long id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement("SELECT ID,BALANCE FROM ACCOUNT WHERE ID=?");
            statement.setLong(1, id);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final BigDecimal balance = resultSet.getBigDecimal("BALANCE");
                return new Account(id, balance);
            }
            throw new AccountNotFoundException(id);
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);// TODO: change
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }

    @Override
    public void transferMoney(Transfer transfer) {
        final long fromAccountId = transfer.getFromAccountId();
        final long toAccountId = transfer.getToAccountId();
        final BigDecimal sum = transfer.getSum();
        if (fromAccountId == toAccountId) {
            throw new FromAndToAccountsTheSameException(fromAccountId);
        }
        if (notPositive(sum)) {
            throw new IncorrectAmountException(sum);
        }
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement("SELECT ID,BALANCE FROM ACCOUNT WHERE ID in (?,?) FOR UPDATE", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            statement.setLong(1, transfer.getFromAccountId());
            statement.setLong(2, transfer.getToAccountId());
            final ResultSet resultSet = statement.executeQuery();
            boolean from = false;
            boolean to = false;
            while (resultSet.next()) {
                final long id = resultSet.getLong("ID");
                final BigDecimal balance = resultSet.getBigDecimal("BALANCE");

                if (id == fromAccountId) {
                    from = true;
                    if (balance.compareTo(sum) < 0) {
                        rollbackTransaction(connection);
                        throw new NotEnoughMoneyException(fromAccountId, new BigDecimal(balance.toString()), new BigDecimal(sum.toString()));
                    }
                    resultSet.updateBigDecimal("BALANCE", balance.subtract(sum));
                } else if (id == toAccountId) {
                    to = true;
                    resultSet.updateBigDecimal("BALANCE", balance.add(sum));
                }
                resultSet.updateRow();
            }
            if (!from) {
                throw new AccountNotFoundException(fromAccountId);
            }
            if (!to) {
                throw new AccountNotFoundException(toAccountId);
            }
            connection.commit();
        } catch (SQLException e) {
            rollbackTransaction(connection);
            log.error(e.getMessage());
            throw new RuntimeException(e);// TODO: change
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }

    @Override
    public Collection<Account> getAll() {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery("SELECT ID,BALANCE FROM ACCOUNT");
            final Collection<Account> result = new LinkedHashSet<>();
            while (resultSet.next()) {
                final long id = resultSet.getLong("ID");
                final BigDecimal balance = resultSet.getBigDecimal("BALANCE");
                result.add(new Account(id, balance));
            }
            return result;
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);// TODO: change
        } finally {
            closeStatement(statement);
            closeConnection(connection);
        }
    }

    private boolean notPositive(BigDecimal sum) {
        return BigDecimal.ZERO.compareTo(sum) >= 0;
    }

    private void rollbackTransaction(Connection connection) {
        if (connection == null) {
            return;
        }
        try {
            connection.rollback();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    private void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
        }
    }

}
