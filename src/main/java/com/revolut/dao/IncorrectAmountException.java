package com.revolut.dao;

import java.math.BigDecimal;

public class IncorrectAmountException extends RuntimeException {

    public final BigDecimal amount;

    public IncorrectAmountException(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
