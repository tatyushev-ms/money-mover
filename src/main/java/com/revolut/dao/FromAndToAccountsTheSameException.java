package com.revolut.dao;

public class FromAndToAccountsTheSameException extends RuntimeException {

    public final long accountId;

    public FromAndToAccountsTheSameException(long accountId) {
        this.accountId = accountId;
    }

    public long getAccountId() {
        return accountId;
    }

}
