package com.revolut;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SimpleResponseShaper implements ResponseShaper {

    private final Gson gson;

    public SimpleResponseShaper(Gson gson) {
        this.gson = gson;
    }

    @Override
    public void sendAsJson(HttpServletResponse resp, Object obj) throws IOException {
        writeJson(resp, obj);
        resp.getWriter().flush();
    }

    @Override
    public void respondWithStatus(HttpServletResponse resp, int status) throws IOException {
        resp.setContentType("application/json");
        setStatus(resp, status);
        resp.getWriter().flush();
    }

    @Override
    public void respondWithStatusAndJsonBody(HttpServletResponse resp, int status, Object obj) throws IOException {
        setStatus(resp, status);
        writeJson(resp, obj);
        resp.getWriter().flush();
    }

    private void writeJson(HttpServletResponse resp, Object obj) throws IOException {
        final String responseBody;
        try {
            responseBody = gson.toJson(obj);
        } catch (Exception e) {
            respondWithStatus(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        resp.setContentType("application/json");
        resp.getWriter().print(responseBody);
    }

    private void setStatus(HttpServletResponse resp, int status) {
        resp.setStatus(status);
    }

}
