# Money mover

## tests
```
mvn clean && mvn test
```


## integration tests
```
mvn clean && mvn verify 
```


## package
```
mvn clean && mvn package
```


## run
```
mvn clean && mvn package && sh -c 'cd ./target && java -jar money-mover-0.0.1-SNAPSHOT.jar'
```


## endpoints

#### account info
```
curl -i http://localhost:8080/money-mover/account/1
```

#### accounts
```
curl -i http://localhost:8080/money-mover/account/
```

#### transfer
```
curl -i -d '{"sum":.1, "fromAccountId":1, "toAccountId":2}' -H 'Content-Type: application/json' -X POST http://localhost:8080/money-mover/transfer
```
